let SuccessResponse = (listoftodo) => {
    return({
        success: true,
        result: listoftodo
    });
}

module.exports = {
    SuccessResponse
}