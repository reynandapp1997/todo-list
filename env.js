let conf;
if (process.env.NODE_ENV === 'dev') {
    conf = {
        uri: 'mongodb://127.0.0.1:27017/todo-list',
        dom: 'http://localhost:3000'
    };
} else if (process.env.NODE_ENV === 'test') {
    conf = {
        uri: 'mongodb://127.0.0.1:27017/todolisttesting',
        dom: 'http://localhost:3000'
    };
}
module.exports = {
    MONGODB_URI: conf.uri,
    DOMAIN: conf.dom
};
