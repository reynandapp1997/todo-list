var mongoose = require('mongoose')

var todoSchema = mongoose.Schema({
    List: String,
    isComplete: {
        type: Boolean,
        default: false
    },
    UserId: {
        type: mongoose.SchemaTypes.ObjectId,
        require: true,
        ref: 'User'
    }
},
{
    collection: 'listoftodo'
})

var listoftodo = mongoose.model('listoftodo', todoSchema)

module.exports = listoftodo