const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');

const should = chai.should();
const expect = chai.expect;
chai.use(chaiHttp);

const User = require('../models/user');
let userId;
let token;

describe('Users', () => {
    before(done => {
        User.deleteMany({}, err => {
            done();
        });
    });

    describe('/POST user', () => {
        it('it should add one user', done => {
            chai.request(server)
                .post('/api/user/add-user')
                .send({
                    name: 'Reynanda Putra Pratama',
                    email: 'reynandapp1997@gmail.com',
                    password: 'gegewepe'
                })
                .end((err, res) => {
                    expect(res.status).eql(201);
                    done();
                });
        });
        it('it should fail add one user with exist email', done => {
            chai.request(server)
                .post('/api/user/add-user')
                .send({
                    name: 'Reynanda Putra Pratama',
                    email: 'reynandapp1997@gmail.com',
                    password: 'gegewepe'
                })
                .end((err, res) => {
                    expect(res.status).eql(409);
                    done();
                });
        });
        it('it should login with status success', done => {
            chai.request(server)
                .post('/api/user/login')
                .send({
                    email: 'reynandapp1997@gmail.com',
                    password: 'gegewepe'
                })
                .end((err, res) => {
                    token = res.header.authorization;
                    expect(res.status).eql(200);
                    done();
                });
        });
        it('it should login with status email not registered', done => {
            chai.request(server)
                .post('/api/user/login')
                .send({
                    email: 'reynandapp1997@gmail.come',
                    password: 'gegewepe'
                })
                .end((err, res) => {
                    expect(res.status).eql(404);
                    done();
                });
        });
        it('it should login with status wrong password', done => {
            chai.request(server)
                .post('/api/user/login')
                .send({
                    email: 'reynandapp1997@gmail.com',
                    password: 'gegewepee'
                })
                .end((err, res) => {
                    expect(res.status).eql(401);
                    done();
                });
        });
    });

    describe('/GET user', () => {
        it('it should get all the users', done => {
            chai.request(server)
                .get('/api/user')
                .end((err, res) => {
                    expect(res.status).eql(200);
                    userId = res.body.user[0].id;
                    console.log(userId);
                    done();
                });
        });
        it('it should get single user', done => {
            chai.request(server)
                .get(`/api/user/${userId}`)
                .end((err, res) => {
                    expect(res.status).eql(200);
                    done();
                });
        });
        it('it should get 404 message for single user', done => {
            chai.request(server)
                .get('/api/user/5d4a2fc902f6870017a7d009')
                .end((err, res) => {
                    expect(res.status).eql(404);
                    done();
                });
        });
    });

    describe('/PUT user', () => {
        it('it should update exist user', done => {
            chai.request(server)
                .put(`/api/user/${userId}`)
                .send({
                    name: 'Reynanda Putra Pratama',
                    email: 'reynandapp1997@gmail.com',
                    password: 'gegewepe'
                })
                .set('Authorization', token)
                .end((err, res) => {
                    expect(res.status).eql(201);
                    done();
                });
        });
        it('it should not update unexist user', done => {
            chai.request(server)
                .put(`/api/user/5d4a2fc902f6870017a7d009`)
                .send({
                    name: 'Reynanda Putra Pratama',
                    email: 'reynandapp1997@gmail.com',
                    password: 'gegewepe'
                })
                .set('Authorization', token)
                .end((err, res) => {
                    expect(res.status).eql(404);
                    done();
                });
        });
    });

    describe('/DELETE user', () => {
        it('it should delete exist user', done => {
            chai.request(server)
                .delete(`/api/user/${userId}`)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(res.status).eql(201);
                    done();
                });
        });
        it('it should not delete unexist user', done => {
            chai.request(server)
                .delete(`/api/user/5d4a2fc902f6870017a7d009`)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(res.status).eql(404);
                    done();
                });
        });
    });
});
