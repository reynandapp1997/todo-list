var collections = require('../models/todolistModels')
var helperRespon = require('../helper/respondhandler.')

let GetList = function(req,res){
    collections.find().populate("UserId", 'name').exec()
        .then((listoftodo) => {
            res.json(
                helperRespon.SuccessResponse(listoftodo)
            );
        });
}

let PostList = function(req,res){
    console.log(req.user.id)
    const newPost = new collections({
        List: req.body.List,
        UserId: req.user.id
    });
    newPost.save((collections) => {
        res.json(
            helperRespon.SuccessResponse(collections)
        )
    })
}

let PutList = function(req,res){
    collections.findByIdAndUpdate(req.params._id,
        {
            $set: req.body
        },  function (err,collections){
            if (err) throw err;
            res.status(200).send(collections);
        })
}

let DeleteList = function(req,res){
    collections.findByIdAndDelete(req.params._id,
        {
            $set: req.body
        },  function (err,collections){
            if (err) throw err;
            res.status(200).send(collections);
        })
}

module.exports = {
    GetList,
    PostList,
    PutList,
    DeleteList
}