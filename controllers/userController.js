const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

const User = require('../models/user');

exports.getUser = (req, res, next) => {
    User.find()
        .sort({
            createdAt: -1
        })
        .select('-password -__v -createdAt -updatedAt')
        .exec()
        .then(result => {
            const userData = result.map(el => {
                return {
                    id: el.id,
                    name: el.name,
                    email: el.email
                };
            });
            return res.status(200).json({
                user: userData,
                length: userData.length
            });
        })
        .catch(error => {
            return res.status(500).json({
                message: 'An error occured',
                errorMessage: error.toString()
            });
        });
};

exports.getUserById = (req, res, next) => {
    const id = req.params.userid;
    User.findById(id)
        .select('-password -__v -createdAt -updatedAt')
        .exec()
        .then(result => {
            if (!result) {
                return res.status(404).json({
                    message: 'User not found'
                });
            }
            const userData = {
                id: result.id,
                name: result.name,
                email: result.email
            };
            return res.status(200).json({
                user: userData
            });
        })
        .catch(error => {
            return res.status(500).json({
                message: 'An error occured',
                errorMessage: error.toString()
            });
        });
};

exports.addUser = async (req, res, next) => {
    const {
        name,
        email,
        password
    } = req.body;
    const userExist = await User.findOne({
        email
    });
    if (userExist) {
        return res.status(409).json({
            message: 'User already registered'
        });
    }
    bcryptjs.hash(password, 10, (error, hash) => {
        if (error) {
            return res.status(500).json({
                message: 'An error occured',
                errorMessage: error.toString()
            });
        }
        const user = new User({
            name,
            email,
            password: hash
        });
        user.save()
            .then(result => {
                return res.status(201).json({
                    message: 'User added'
                });
            })
            .catch(err => {
                return res.status(500).json({
                    message: 'An error occured',
                    errorMessage: err.toString()
                });
            });
    });
};

exports.login = async (req, res, next) => {
    const {
        email,
        password
    } = req.body;
    const userExist = await User.findOne({
        email
    });
    if (!userExist) {
        return res.status(404).json({
            message: 'User not registered'
        });
    }
    bcryptjs.compare(password, userExist.password, async (error, success) => {
        if (error) {
            return res.status(500).json({
                message: 'An error occured',
                errorMessage: error.toString()
            });
        } else if (success) {
            const token = jwt.sign({
                id: userExist.id,
                name: userExist.name,
                email: userExist.email
            }, 'bankindonesiajsonwebtokensecret', {
                expiresIn: '24h'
            });
            res.setHeader('Authorization', `Bearer ${token}`);
            await User.findByIdAndUpdate({
                _id: userExist.id
            }, {
                $set: {
                    isChanged: true
                }
            });
            return res.status(200).json({
                message: 'Login berhasil',
                id: userExist.id,
                name: userExist.name,
                email: userExist.email,
                token
            });
        }
        return res.status(401).json({
            message: 'Kata sandi anda salah'
        });
    });
};

exports.updateUser = async (req, res, next) => {
    const id = req.params.userid;
    const userExist = await User.findById(id);
    if (!userExist) {
        return res.status(404).json({
            message: 'User not registered'
        });
    }
    const {
        name,
        email,
        password
    } = req.body;
    const hash = await bcryptjs.hash(password, 10);
    const userData = new User({
        _id: id,
        name,
        email,
        password: password ? hash : undefined
    });
    User.findByIdAndUpdate({
        _id: id
    }, userData, (error, result) => {
        if (error) {
            return res.status(500).json({
                message: 'An error occured',
                errorMessage: error.toString()
            });
        }
        return res.status(201).json({
            message: 'User updated'
        });
    });
};

exports.deleteUser = async (req, res, next) => {
    const id = req.params.userid;
    const user = await User.findById(id);
    if (!user) {
        return res.status(404).json({
            message: 'User not registered'
        });
    }
    User.findByIdAndDelete({
        _id: id
    }, async (error, result) => {
        if (error) {
            return res.status(500).json({
                message: 'An error occured',
                errorMessage: error.toString()
            });
        }
        const userid = mongoose.mongo.ObjectId(req.params.userid);
        res.status(201).json({
            message: 'User deleted'
        });
    });
};
