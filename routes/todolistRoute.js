var express = require('express');
var router = express.Router();
var todoController = require('../controllers/todolistController')
var auth = require('../middlewares/auth')

router.get('/', todoController.GetList);

router.post('/', auth, todoController.PostList);

router.put('/:_id', todoController.PutList);

router.delete('/:_id', todoController.DeleteList);

module.exports = router;