const express = require('express');
const router = express.Router();

const {
    getUser,
    getUserById,
    addUser,
    login,
    updateUser,
    deleteUser
} = require('../controllers/userController');
const auth = require('../middlewares/auth');

router.get('/', getUser);

router.get('/:userid', getUserById);

router.post('/add-user', addUser);

router.post('/login', login);

router.put('/:userid', auth, updateUser);

router.delete('/:userid', auth, deleteUser);

module.exports = router;
