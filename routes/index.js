var express = require('express');
var router = express.Router();

var userRoute = require('./userRoute');
var todolistRoute = require('./todolistRoute')

// add new route below this
router.use('/user', userRoute);
router.use('/todo', todolistRoute)

module.exports = router;
